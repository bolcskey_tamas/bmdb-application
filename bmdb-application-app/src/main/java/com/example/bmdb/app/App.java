package com.example.bmdb.app;

import java.math.BigDecimal;
import java.util.List;

import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;

public class App {

	private Review review;
	private List<Media> medias;
	private Media selectedMedia;
	private User currentUser;
	private Service service;
	private View view;
	
	public App(Service service, View view) {
		super();
		this.service = service;
		this.view = view;
		this.medias = service.findAllMedia();
	}
	
	public void Play(){
		createUser();
		view.printWelcomeMessage(currentUser);
		doReview();
	}
	
	private void createUser() {
		currentUser = view.readUserData();
		service.saveUser(currentUser);
	}
	
	private void doReview(){
		do {
			view.printMedias(medias);
			BigDecimal id = view.readId();
			selectedMedia = medias.stream()
					.filter(x->x.getId().equals(id))
					.findFirst()
					.get();
			String text = view.readReviewText();
			Rating rating = view.readRating();
			review = new Review.Builder()
					.setCreator(currentUser)
					.setMedia(selectedMedia)
					.setRating(rating)
					.setText(text)
					.Build();
			service.saveReview(review.getMedia(), review);
			printReviewAverage();
		} while (view.readDone().equalsIgnoreCase("yes"));
			
	}
	
	private void printReviewAverage(){
		double average = selectedMedia
				.getReviews()
				.stream()
				.map(x->Rating.valueOfRating(x.getRating()))
				.mapToDouble(x->x)
				.average().orElse(0);
		System.out.println("Average of reviews: "+ average);
	}

	public static void main(String[] args) {
		App app = new App(new Service(),new View());
		app.Play();
	}

	
	
	
	
	

}
