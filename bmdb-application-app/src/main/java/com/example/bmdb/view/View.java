package com.example.bmdb.view;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import com.example.bmdb.domain.Actor;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;

public class View {

	static Scanner input = new Scanner(System.in);

	public User readUserData() {

		System.out.println("What is your name?");
		String name = input.nextLine();
		System.out.println("What is your email address?");
		String email = input.nextLine();
		System.out.println("What is your password?");
		String password = input.nextLine();
		return new User.Builder().setName(name).setEmail(email).setPassWord(password).Build();
	}

	public void printWelcomeMessage(User user) {
		System.out.println("Welcome to the movie and series review application, " + user.getName() + "!");
	}

	public void printMedias(List<Media> medias) {
		StringBuilder sb = new StringBuilder();
		for(Media media : medias) {
			sb.append("Id: "+media.getId()+"\n")
			.append("Title: "+media.getTitle()+"\n")
			.append("Description: "+media.getDescription()+"\n")
			.append("Premier: "+media.getPremier()+"\n")
			.append("Cast:\n");
			for(Actor actor : media.getActors()) {
				sb.append("\t"+actor.toString()+"\n");
			}
			sb.append("Reviews:\n");
			for(Review review: media.getReviews()) {
				sb.append("\t"+review.toString()+"\n");
			}
		}
		System.out.println(sb.toString());
	}

	public void printReview(User user) {
		StringBuilder sb = new StringBuilder();
		for (Review review : user.getReviews()) {
			sb.append(review.toString() + "\n");
		}
		System.out.println(sb.toString());
	}

	public BigDecimal readId() {
		System.out.println("Choose an Id:");
		BigDecimal id = new BigDecimal(input.nextLine());
		return id;
	}

	public String readReviewText() {
		System.out.println("Write a review:");
		String text = input.nextLine();
		return text;
	}

	public Rating readRating() {
		System.out.println("Choose a rating (bad/average/good):");
		Rating rating = Rating.valueOf(input.nextLine().toUpperCase());
		return rating;
	}

	public String readDone() {
		System.out.println("Do you want to write another review? (yes/no)");
		String choice = input.nextLine();
		return choice;
	}
}
