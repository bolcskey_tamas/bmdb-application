package com.example.bmdb.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.bmdb.domain.Actor;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Movie;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.Series;
import com.example.bmdb.domain.Sex;
import com.example.bmdb.domain.User;

public class Service {
	private List<User> users;
	private List<Media> medias;

	public Service() {
		this.users = new ArrayList<User>();
		this.medias = new ArrayList<Media>();
		createTestData();
	}

	public void saveUser(User user) {
		if (!users.contains(user)) {
			users.add(user);
		}
	}

	public List<Media> findAllMedia() {
		return medias;
	}

	public User findUser(String name) {
		User user = users.stream().filter(x -> x.getName() == name).findFirst().get();
		return user;
	}

	public void saveReview(Media mediaToReview, Review review) {
		Media media = medias.stream().filter(x -> x.getId() == mediaToReview.getId()).findFirst().get();
		media.getReviews().add(review);
	}

	private void createTestData() {
		List<Actor> actors = new ArrayList<Actor>();
		actors.add(new Actor.Builder().setBiography("Biography of Actor0").setName("Actor0").setBorn(LocalDate.of(1962, 10, 12)).setSex(Sex.MALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor1").setName("Actor1").setBorn(LocalDate.of(1989, 7, 20)).setSex(Sex.FEMALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor2").setName("Actor2").setBorn(LocalDate.of(1980, 5, 9)).setSex(Sex.MALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor3").setName("Actor3").setBorn(LocalDate.of(1999, 10, 30)).setSex(Sex.MALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor4").setName("Actor4").setBorn(LocalDate.of(1978, 3, 28)).setSex(Sex.FEMALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor5").setName("Actor5").setBorn(LocalDate.of(1956, 12, 24)).setSex(Sex.MALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor6").setName("Actor6").setBorn(LocalDate.of(1994, 11, 20)).setSex(Sex.FEMALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor7").setName("Actor7").setBorn(LocalDate.of(1935, 4, 11)).setSex(Sex.MALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor8").setName("Actor8").setBorn(LocalDate.of(2000, 3, 22)).setSex(Sex.FEMALE).Build());
		actors.add(new Actor.Builder().setBiography("Biography of Actor9").setName("Actor9").setBorn(LocalDate.of(1991, 6, 7)).setSex(Sex.MALE).Build());
		
		medias.add(new Movie.Builder()
				.addActor(actors.get(0))
				.addActor(actors.get(8))
				.addActor(actors.get(3))
				.setId(new BigDecimal(0))
				.setDescription("Description of Media0")
				.setTitle("Media0")
				.setPremier(LocalDate.of(2002,12,1))
				.Build());
		medias.add(new Series.Builder()
				.addActor(actors.get(1))
				.addActor(actors.get(3))
				.addActor(actors.get(8))
				.setId(new BigDecimal(1))
				.setDescription("Description of Media1")
				.setTitle("Media1")
				.setPremier(LocalDate.of(2010,2,11))
				.Build());
		medias.add(new Movie.Builder()
				.addActor(actors.get(2))
				.addActor(actors.get(9))
				.addActor(actors.get(6))
				.addActor(actors.get(1))
				.setId(new BigDecimal(2))
				.setDescription("Description of Media2")
				.setTitle("Media2")
				.setPremier(LocalDate.of(2020,4,21))
				.Build());
		medias.add(new Series.Builder()
				.addActor(actors.get(9))
				.addActor(actors.get(7))
				.addActor(actors.get(4))
				.addActor(actors.get(5))
				.setId(new BigDecimal(3))
				.setDescription("Description of Media3")
				.setTitle("Media3")
				.setPremier(LocalDate.of(2009,6,30))
				.Build());
		medias.add(new Movie.Builder()
				.addActor(actors.get(5))
				.addActor(actors.get(6))
				.addActor(actors.get(7))
				.addActor(actors.get(8))
				.setId(new BigDecimal(4))
				.setDescription("Description of Media4")
				.setTitle("Media4")
				.setPremier(LocalDate.of(2014,6,9))
				.Build());
		users.add(new User.Builder().setEmail("user1@email1.com").setName("User0").setPassWord("pass0").Build());
		users.add(new User.Builder().setEmail("user2@email1.com").setName("User1").setPassWord("pass1").Build());
		users.add(new User.Builder().setEmail("user3@email1.com").setName("User2").setPassWord("pass2").Build());
		
		Review review0 = new Review.Builder().setCreator(users.get(0)).setMedia(medias.get(0)).setRating(Rating.GOOD)
				.setText("good").Build();
		Review review1 = new Review.Builder().setCreator(users.get(0)).setMedia(medias.get(0)).setRating(Rating.GOOD)
				.setText("good").Build();
		Review review2 = new Review.Builder().setCreator(users.get(1)).setMedia(medias.get(1)).setRating(Rating.BAD)
				.setText("bad").Build();
		Review review3 = new Review.Builder().setCreator(users.get(2)).setMedia(medias.get(3)).setRating(Rating.BAD)
				.setText("bad").Build();
		Review review4 = new Review.Builder().setCreator(users.get(0)).setMedia(medias.get(3)).setRating(Rating.AVERAGE)
				.setText("average").Build();
		Review review5 = new Review.Builder().setCreator(users.get(1)).setMedia(medias.get(2)).setRating(Rating.AVERAGE)
				.setText("average").Build();
		Review review6 = new Review.Builder().setCreator(users.get(2)).setMedia(medias.get(4)).setRating(Rating.GOOD)
				.setText("good").Build();
		
		saveReview(review0.getMedia(), review0);
		saveReview(review1.getMedia(), review1);
		saveReview(review2.getMedia(), review2);
		saveReview(review3.getMedia(), review3);
		saveReview(review4.getMedia(), review4);
		saveReview(review5.getMedia(), review5);
		saveReview(review6.getMedia(), review6);
	}

}
