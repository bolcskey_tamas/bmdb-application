package com.example.bmdb.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Actor {

	private String name;

	private LocalDate born;

	private String biography;

	private Sex sex;

	private List<Media> filmography;

	private Actor(Builder builder) {
		super();
		this.name = builder.name;
		this.born = builder.born;
		this.biography = builder.biography;
		this.sex = builder.sex;
		this.filmography = builder.filmography;
	}

	@Override
	public String toString() {
		return "Actor [name=" + name + ", born=" + born + ", biography=" + biography + "]";
	}

	public String getName() {
		return name;
	}

	public LocalDate getBorn() {
		return born;
	}

	public String getBiography() {
		return biography;
	}

	public Sex getSex() {
		return sex;
	}

	public List<Media> getFilmography() {
		return filmography;
	}

	public static class Builder {
		private String name;
		private LocalDate born;
		private String biography;
		private Sex sex;
		private List<Media> filmography;

		public Builder() {
			this.filmography = new ArrayList<Media>();
		}

		public Builder addFilm(Media media) {
			this.filmography.add(media);
			return this;
		}

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setBorn(LocalDate born) {
			this.born = born;
			return this;
		}

		public Builder setBiography(String biography) {
			this.biography = biography;
			return this;
		}

		public Builder setSex(Sex sex) {
			this.sex = sex;
			return this;
		}

		public Actor Build() {
			return new Actor(this);
		}

	}

}
