package com.example.bmdb.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Media {

	private BigDecimal id;

	private String title;

	private String description;

	private LocalDate premier;

	private List<Actor> actors;

	private List<Review> reviews;

	public Media(Builder builder) {
		super();
		this.id = builder.id;
		this.title = builder.title;
		this.description = builder.description;
		this.premier = builder.premier;
		this.reviews = builder.reviews;
		this.actors = builder.actors;
	}

	@Override
	public String toString() {
		return "Media [id=" + id + ", title=" + title + ", description=" + description + ", premier=" + premier + "]";
	}

	public BigDecimal getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public LocalDate getPremier() {
		return premier;
	}

	public List<Actor> getActors() {
		return actors;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actors == null) ? 0 : actors.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((premier == null) ? 0 : premier.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Media other = (Media) obj;
		if (actors == null) {
			if (other.actors != null)
				return false;
		} else if (!actors.equals(other.actors))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (premier == null) {
			if (other.premier != null)
				return false;
		} else if (!premier.equals(other.premier))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public static class Builder {
		private BigDecimal id;

		private String title;

		private String description;

		private LocalDate premier;

		private List<Actor> actors;

		private List<Review> reviews;

		public Builder() {
			this.actors = new ArrayList<Actor>();
			this.reviews = new ArrayList<Review>();
		}

		public Builder addActor(Actor actor) {
			this.actors.add(actor);
			return this;
		}

		public Builder addReview(Review review) {
			this.reviews.add(review);
			return this;
		}

		public Builder setId(BigDecimal id) {
			this.id = id;
			return this;
		}

		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder setDescription(String description) {
			this.description = description;
			return this;
		}

		public Builder setPremier(LocalDate premier) {
			this.premier = premier;
			return this;
		}

		public Media Build() {
			return new Media(this);
		}
	}

}
